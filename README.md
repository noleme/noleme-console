# Noleme Console

_Last updated for v1.4.1_

This library is meant as framework for building CLI tools.

It is split in two structural groups:

* the `Task` classes which are meant to define routines with pre-defined input requirements
* the `Console` components which handle the routine runtime execution and routing

Together, they help define a console as a program with a list of pre-defined routines that each accept required and optional parameters.
Some specific components can help with more specific tasks such as handling looping executions (`com.noleme.console.loop`) or launching a small HTTP server that can be used as an entry-point (WIP).   

Implementations found in this package will obviously be opinionated but shouldn't be tied to a specific Noleme project.

_Note: This library is considered as "in production" and as such it should respect semantic versioning and good practices for backward compatibility._

## I. Installation

Add the following in your `pom.xml`:

```xml
<dependency>
    <groupId>com.noleme</groupId>
    <artifactId>noleme-console</artifactId>
    <version>1.4.1</version>
</dependency>
```

## II. Notes on Structure and Design

_TODO_

## III. Usage

_TODO_

## IV. Dev Installation

### A. Pre-requisites

This project will require you to have the following:

* Git (versioning)
* Maven (dependency resolving, publishing and packaging) 

### B. Setup

_TODO_
