package com.noleme.console;

import com.noleme.console.context.Context;
import com.noleme.console.exception.ConsoleException;
import com.noleme.console.output.Output;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 27/10/2017
 */
final public class ConsoleRunner
{
    /**
     *
     */
    private ConsoleRunner()
    {

    }

    /**
     *
     * @param console
     * @param args
     * @return
     */
    public static int run(Console console, String[] args)
    {
        return run(console, args, false);
    }

    /**
     *
     * @param console
     * @param args
     */
    public static int run(Console console, String[] args, boolean printStackTrace)
    {
        int statusCode = 0;

        Context context = new Context()
            .setIncludeContext(true)
        ;
        Output output = context.getOutput();

        try {
            console.run(args, context);
        }
        catch (ConsoleException e) {
            output.block.error(e.getMessage());

            Throwable cause = e.getCause();
            while (cause != null)
            {
                output.println("-- "+cause.getClass().getName()+": "+((cause.getMessage() != null) ? cause.getMessage() : "<no message>"));

                if (printStackTrace)
                {
                    for (StackTraceElement el : cause.getStackTrace())
                        output.println("   - at "+el.getClassName()+"."+el.getMethodName()+" l:"+el.getLineNumber());
                }

                cause = cause.getCause();
            }

            statusCode = 1;
        }
        finally {
            output.println("\nExiting.");
        }

        return statusCode;
    }
}
