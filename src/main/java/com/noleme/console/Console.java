package com.noleme.console;

import com.noleme.console.context.Context;
import com.noleme.console.context.ContextProvider;
import com.noleme.console.context.history.task.event.TaskStatus;
import com.noleme.console.exception.ConsoleException;
import com.noleme.console.exception.TaskNotFoundException;
import com.noleme.console.input.Input;
import com.noleme.console.input.InputParser;
import com.noleme.console.loop.task.PlannedTask;
import com.noleme.console.runtime.ConsoleRuntime;
import com.noleme.console.task.Help;
import com.noleme.console.task.List;
import com.noleme.console.task.Task;
import com.noleme.console.task.description.TaskDescription;
import com.noleme.console.task.exception.TaskException;

import java.util.*;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 29/04/2017
 */
public class Console implements ConsoleRuntime
{
    private final Map<String, Task> tasks;
    private final Map<String, TaskDescription> taskDescriptions;
    private final InputParser parser;
    private ContextProvider defaultContextProvider;

    public Console()
    {
        this.tasks = new HashMap<>();
        this.taskDescriptions = new HashMap<>();
        this.parser = new InputParser();
        this.defaultContextProvider = Context::new;
        this.setTask("list", new List());
        this.setTask("help", new Help());
    }

    /**
     *
     * @param args
     * @throws ConsoleException
     */
    public void run(String[] args) throws ConsoleException
    {
        this.run(args, new Context());
    }

    /**
     *
     * @param args
     * @param context
     * @throws ConsoleException
     */
    public void run(String[] args, Context context) throws ConsoleException
    {
        if (args.length == 0)
            this.run("list", context);
        else
            this.run(args[0], context, args.length > 1 ? Arrays.copyOfRange(args, 1, args.length) : new String[0]);
    }

    @Override
    public void run(PlannedTask planned) throws ConsoleException
    {
        String name = planned.getTask();
        String[] args = planned.getArgumentProvider().provide();
        Context context = (planned.getContextProvider() == null
            ? this.defaultContextProvider
            : planned.getContextProvider()
        ).provide();
        String uid = UUID.randomUUID().toString();

        try {
            if (!this.hasTask(name))
                throw new TaskNotFoundException("No task named '"+name+"' has been defined on this console.");

            Task task = this.getTask(name);
            TaskDescription description = this.getTaskDescription(name);

            if (context.doesIncludeContext())
            {
                context.getOutput().block.info("Running \""+name+"\" : "+(description != null ? description.getDescription() : "~"));

                /* Handle deprecated mentions */
                Deprecated deprecated = task.getClass().getAnnotation(Deprecated.class);
                if (deprecated != null)
                    context.getOutput().block.warning("This task is marked as deprecated");
            }

            Input input = this.parser.parse(args, description);

            context.history(name).register(uid, args, TaskStatus.STARTED);

            this.run(task, input, context, uid);

            context.history(name).register(uid, args, TaskStatus.FINISHED);
        }
        catch (TaskException | RuntimeException e) {
            context.history(name).register(uid, args, e);
            throw new ConsoleException("The '"+name+"' task encountered an error.", e);
        }
        catch (ConsoleException e) {
            context.history(name).register(uid, args, e);
            throw e;
        }
    }

    /**
     * Extension point for the task execution (allows the replacement of adjustment of the runtime, context, additional logging, etc)
     *
     * @param task
     * @param input
     * @param context
     * @param uid
     * @throws TaskException
     */
    protected void run(Task task, Input input, Context context, String uid) throws TaskException
    {
        task.run(this, input, context.getOutput());
    }

    /**
     *
     * @param provider
     * @return
     */
    public Console setDefaultContextProvider(ContextProvider provider)
    {
        this.defaultContextProvider = provider;
        return this;
    }

    /**
     *
     * @param name
     * @param task
     */
    public Console setTask(String name, Task task)
    {
        this.tasks.put(name, task);
        this.taskDescriptions.put(name, task.setup());
        return this;
    }

    /**
     *
     * @param alias
     * @param name
     * @return
     * @throws ConsoleException
     */
    public Console setAlias(String alias, String name) throws ConsoleException
    {
        if (!this.hasTask(name))
            throw new ConsoleException("Cannot define alias "+alias+" to an undeclared "+name+" task.");

        this.setTask(alias, this.getTask(name));
        return this;
    }

    /**
     *
     * @param name
     * @return
     */
    public boolean hasTask(String name)
    {
        return this.tasks.containsKey(name);
    }

    /**
     *
     * @param name
     * @return
     */
    public Task getTask(String name)
    {
        return this.tasks.get(name);
    }

    /**
     *
     * @param name
     * @return
     */
    public TaskDescription getTaskDescription(String name)
    {
        return this.taskDescriptions.get(name);
    }

    /**
     *
     * @return
     */
    public Set<String> listTasks()
    {
        return this.tasks.keySet();
    }
}
