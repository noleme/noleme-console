package com.noleme.console.output;

import com.noleme.console.output.print.block.BlockPrinter;
import com.noleme.console.output.print.color.ColorPrinter;
import com.noleme.console.output.print.line.LinePrinter;
import com.noleme.console.output.writer.Writer;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/12/2019
 */
public abstract class Output
{
    private final Writer writer = this.setupWriter();
    /* Printers */
    public final ColorPrinter color = this.setupColorPrinter(this.writer);
    public final LinePrinter line = this.setupLinePrinter(this.writer);
    public final BlockPrinter block = this.setupBlockPrinter(this.writer);

    /**
     *
     * @return
     */
    protected abstract Writer setupWriter();

    /**
     *
     * @param writer
     * @return
     */
    protected abstract LinePrinter setupLinePrinter(Writer writer);

    /**
     *
     * @param writer
     * @return
     */
    protected abstract BlockPrinter setupBlockPrinter(Writer writer);

    /**
     *
     * @param writer
     * @return
     */
    protected abstract ColorPrinter setupColorPrinter(Writer writer);

    /**
     *
     * @return
     */
    public Output println()
    {
        this.writer.println();
        return this;
    }

    /**
     *
     * @param string
     * @return
     */
    public Output println(String string)
    {
        this.writer.println(string);
        return this;
    }

    /**
     *
     * @param object
     * @return
     */
    public Output println(Object object)
    {
        this.writer.println(object);
        return this;
    }

    /**
     * 
     * @param string
     * @return
     */
    public Output print(String string)
    {
        this.writer.print(string);
        return this;
    }

    /**
     *
     * @param object
     * @return
     */
    public Output print(Object object)
    {
        this.writer.print(object);
        return this;
    }
}
