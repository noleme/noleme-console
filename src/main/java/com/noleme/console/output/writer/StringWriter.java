package com.noleme.console.output.writer;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/12/2019
 */
public class StringWriter implements Writer
{
    private final StringBuilder builder;

    public StringWriter()
    {
        this.builder = new StringBuilder();
    }

    @Override
    synchronized public void print(String string)
    {
        this.builder.append(string);
    }

    @Override
    synchronized public void print(Object object)
    {
        this.builder.append(object);
    }

    @Override
    synchronized public void println()
    {
        this.builder.append("\n");
    }

    @Override
    synchronized public void println(String string)
    {
        this.builder.append(string).append("\n");
    }

    @Override
    synchronized public void println(Object object)
    {
        this.builder.append(object).append("\n");
    }

    /**
     *
     * @return
     */
    public StringBuilder builder()
    {
        return this.builder;
    }
}
