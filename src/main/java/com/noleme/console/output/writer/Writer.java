package com.noleme.console.output.writer;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/12/2019
 */
public interface Writer
{
    void print(String string);
    void print(Object object);
    void println();
    void println(String string);
    void println(Object object);
}
