package com.noleme.console.output.writer;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/12/2019
 */
public class ConsoleWriter implements Writer
{
    @Override
    public void print(String string)
    {
        System.out.print(string);
    }

    @Override
    public void print(Object object)
    {
        System.out.print(object);
    }

    @Override
    public void println()
    {
        System.out.println();
    }

    @Override
    public void println(String string)
    {
        System.out.println(string);
    }

    @Override
    public void println(Object object)
    {
        System.out.println(object);
    }
}
