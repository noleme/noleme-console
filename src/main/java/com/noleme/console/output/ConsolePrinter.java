package com.noleme.console.output;

import com.noleme.console.output.print.block.BlockPrinter;
import com.noleme.console.output.print.color.ColorPrinter;
import com.noleme.console.output.print.line.LinePrinter;
import com.noleme.console.output.writer.ConsoleWriter;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 * @deprecated use the Task output instead
 */
@Deprecated
public class ConsolePrinter
{
    public static final ColorPrinter color = new ColorPrinter(new ConsoleWriter());
    public static final LinePrinter line = new LinePrinter(new ConsoleWriter(), color);
    public static final BlockPrinter block = new BlockPrinter(new ConsoleWriter());
}
