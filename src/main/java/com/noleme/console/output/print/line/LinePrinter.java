package com.noleme.console.output.print.line;

import com.noleme.console.output.print.color.ColorPrinter;
import com.noleme.console.output.style.ConsoleColor;
import com.noleme.console.output.writer.Writer;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class LinePrinter
{
    private final Writer writer;
    private final ColorPrinter colorPrinter;

    /**
     *
     * @param writer
     * @param colorPrinter
     */
    public LinePrinter(Writer writer, ColorPrinter colorPrinter)
    {
        this.writer = writer;
        this.colorPrinter = colorPrinter;
    }

    public void info(String message)
    {
        this.line(message, ConsoleColor.CYAN);
    }

    public void success(String message)
    {
        this.line(message, ConsoleColor.GREEN);
    }

    public void warning(String message)
    {
        this.line(message, ConsoleColor.YELLOW);
    }

    public void error(String message)
    {
        this.line(message, ConsoleColor.RED);
    }

    /**
     *
     * @param message
     * @param textColor
     * @deprecated Use {@link #line(String, ConsoleColor)} instead
     */
    @Deprecated
    public void custom(String message, ConsoleColor textColor)
    {
        this.line(message, textColor);
    }

    /**
     *
     * @param message
     * @param textColor
     */
    public void line(String message, ConsoleColor textColor)
    {
        this.writer.println(this.colorPrinter.wrap("~> ", textColor)+message);
    }
}
