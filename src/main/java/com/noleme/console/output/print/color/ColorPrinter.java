package com.noleme.console.output.print.color;

import com.noleme.console.output.style.ConsoleColor;
import com.noleme.console.output.writer.Writer;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class ColorPrinter
{
    private final Writer writer;

    /**
     *
     * @param writer
     */
    public ColorPrinter(Writer writer)
    {
        this.writer = writer;
    }

    /**
     *
     * @param message
     * @param textColor
     * @param backgroundColor
     * @return
     */
    public String wrap(String message, ConsoleColor textColor, ConsoleColor.Background backgroundColor)
    {
        return (textColor != null ? textColor.code : "")+(backgroundColor != null ? backgroundColor.code : "")+message+ConsoleColor.RESET.code;
    }

    /**
     *
     * @param message
     * @param textColor
     * @return
     */
    public String wrap(String message, ConsoleColor textColor)
    {
        return this.wrap(message, textColor, null);
    }

    /**
     *
     * @param message
     * @param backgroundColor
     * @return
     */
    public String wrap(String message, ConsoleColor.Background backgroundColor)
    {
        return this.wrap(message, null, backgroundColor);
    }

    /**
     *
     * @param message
     * @param textColor
     * @param backgroundColor
     */
    public void print(String message, ConsoleColor textColor, ConsoleColor.Background backgroundColor)
    {
        this.writer.print(this.wrap(message, textColor, backgroundColor));

        //TODO: should investigate this, I believe I remember this had to do with "cancelling-out" the effects of bg colour tags from the print behaviour above, hence the
        if (message.isEmpty())
            this.writer.print(this.wrap("", ConsoleColor.BLACK));
    }

    /**
     *
     * @param message
     * @param textColor
     */
    public void print(String message, ConsoleColor textColor)
    {
        this.print(message, textColor, null);
    }

    /**
     *
     * @param message
     * @param backgroundColor
     */
    public void print(String message, ConsoleColor.Background backgroundColor)
    {
        this.print(message, null, backgroundColor);
    }

    /**
     *
     * @param message
     * @param textColor
     * @param backgroundColor
     */
    public void println(String message, ConsoleColor textColor, ConsoleColor.Background backgroundColor)
    {
        this.writer.println(this.wrap(message, textColor, backgroundColor));

        if (message.isEmpty())
            this.writer.print(this.wrap("", ConsoleColor.BLACK));
    }

    /**
     *
     * @param message
     * @param textColor
     */
    public void println(String message, ConsoleColor textColor)
    {
        this.println(message, textColor, null);
    }

    /**
     *
     * @param message
     * @param backgroundColor
     */
    public void println(String message, ConsoleColor.Background backgroundColor)
    {
        this.println(message, null, backgroundColor);
    }
}
