package com.noleme.console.output.print.block;

import com.noleme.console.output.print.color.ColorPrinter;
import com.noleme.console.output.style.ConsoleColor;
import com.noleme.console.output.writer.Writer;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class BlockPrinter
{
    private final ColorPrinter colorPrinter;
    private final Writer writer;

    /**
     *
     * @param writer
     */
    public BlockPrinter(Writer writer)
    {
        this.writer = writer;
        this.colorPrinter = new ColorPrinter(writer);
    }

    /**
     *
     * @param message
     */
    public void info(String message)
    {
        this.block(message, "Info", ConsoleColor.CYAN);
    }

    /**
     *
     * @param message
     */
    public void success(String message)
    {
        this.block(message, "Success", ConsoleColor.GREEN);
    }

    /**
     *
     * @param message
     */
    public void warning(String message)
    {
        this.block(message, "Warning", ConsoleColor.YELLOW);
    }

    /**
     *
     * @param message
     */
    public void error(String message)
    {
        this.block(message, "Error", ConsoleColor.RED);
    }

    /**
     *
     * @param message
     * @param textColor
     * @deprecated Use {@link #block(String, ConsoleColor)} instead
     */
    @Deprecated
    public void custom(String message, ConsoleColor textColor)
    {
        this.block(message, "Log", textColor);
    }

    /**
     *
     * @param message
     * @param textColor
     */
    public void block(String message, ConsoleColor textColor)
    {
        this.block(message, "Log", textColor);
    }

    /**
     *
     * @param message
     * @param label
     * @param textColor
     */
    public void block(String message, String label, ConsoleColor textColor)
    {
        this.writer.println("\n" + this.colorPrinter.wrap("["+label+"] ", textColor) + message + "\n");
    }
}