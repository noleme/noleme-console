package com.noleme.console.output.print.color;

import com.noleme.console.output.style.ConsoleColor;
import com.noleme.console.output.writer.Writer;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 18/12/2019
 */
public class NeutralPrinter extends ColorPrinter
{
    /**
     * @param writer
     */
    public NeutralPrinter(Writer writer)
    {
        super(writer);
    }

    @Override
    public String wrap(String message, ConsoleColor textColor, ConsoleColor.Background backgroundColor)
    {
        return message;
    }
}
