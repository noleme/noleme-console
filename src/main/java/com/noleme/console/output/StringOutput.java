package com.noleme.console.output;

import com.noleme.console.output.print.block.BlockPrinter;
import com.noleme.console.output.print.color.ColorPrinter;
import com.noleme.console.output.print.color.NeutralPrinter;
import com.noleme.console.output.print.line.LinePrinter;
import com.noleme.console.output.writer.StringWriter;
import com.noleme.console.output.writer.Writer;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/12/2019
 */
public class StringOutput extends Output
{
    private StringWriter writer;

    @Override
    protected Writer setupWriter()
    {
        this.writer = new StringWriter();
        return this.writer;
    }

    @Override
    protected LinePrinter setupLinePrinter(Writer writer)
    {
        return new LinePrinter(writer, this.color);
    }

    @Override
    protected BlockPrinter setupBlockPrinter(Writer writer)
    {
        return new BlockPrinter(writer);
    }

    @Override
    protected ColorPrinter setupColorPrinter(Writer writer)
    {
        return new NeutralPrinter(writer);
    }

    public StringBuilder builder()
    {
        return this.writer.builder();
    }
}
