package com.noleme.console.output;

import com.noleme.console.output.print.block.BlockPrinter;
import com.noleme.console.output.print.color.ColorPrinter;
import com.noleme.console.output.print.line.LinePrinter;
import com.noleme.console.output.writer.ConsoleWriter;
import com.noleme.console.output.writer.Writer;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/12/2019
 */
public class ConsoleOutput extends Output
{
    @Override
    protected Writer setupWriter()
    {
        return new ConsoleWriter();
    }

    @Override
    protected LinePrinter setupLinePrinter(Writer writer)
    {
        return new LinePrinter(writer, this.color);
    }

    @Override
    protected BlockPrinter setupBlockPrinter(Writer writer)
    {
        return new BlockPrinter(writer);
    }

    @Override
    protected ColorPrinter setupColorPrinter(Writer writer)
    {
        return new ColorPrinter(writer);
    }
}
