package com.noleme.console.runtime;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 20/12/2019
 */
public interface RuntimeProvider
{
    /**
     *
     * @return
     */
    ConsoleRuntime provide();

    /**
     *
     * @param runtime
     * @return
     */
    static RuntimeProvider provide(ConsoleRuntime runtime)
    {
        return () -> runtime;
    }
}
