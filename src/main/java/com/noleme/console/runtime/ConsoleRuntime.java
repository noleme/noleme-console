package com.noleme.console.runtime;

import com.noleme.console.context.Context;
import com.noleme.console.context.ContextProvider;
import com.noleme.console.exception.ConsoleException;
import com.noleme.console.loop.ArgumentProvider;
import com.noleme.console.loop.task.PlannedTask;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 20/12/2019
 */
public interface ConsoleRuntime
{
    /**
     *
     * @param task
     * @throws ConsoleException
     */
    void run(PlannedTask task) throws ConsoleException;

    /**
     *
     * @param name
     * @throws ConsoleException
     */
    default void run(String name) throws ConsoleException
    {
        this.run(new PlannedTask(name, ArgumentProvider.provideEmpty()));
    }

    /**
     *
     * @param name
     * @param context
     * @throws ConsoleException
     */
    default void run(String name, Context context) throws ConsoleException
    {
        this.run(new PlannedTask(
                name,
                ArgumentProvider.provideEmpty()
            ).setContextProvider(ContextProvider.provide(context))
        );
    }

    /**
     *
     * @param name
     * @param args
     * @throws ConsoleException
     */
    default void run(String name, String... args) throws ConsoleException
    {
        this.run(new PlannedTask(name, ArgumentProvider.provide(args)));
    }

    /**
     *
     * @param name
     * @param context
     * @param args
     * @throws ConsoleException
     */
    default void run(String name, Context context, String... args) throws ConsoleException
    {
        this.run(new PlannedTask(
                name,
                ArgumentProvider.provide(args)
            ).setContextProvider(ContextProvider.provide(context))
        );
    }
}
