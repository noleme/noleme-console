package com.noleme.console.context;

import com.noleme.console.context.history.History;
import com.noleme.console.context.history.HistoryProvider;
import com.noleme.console.context.history.memory.MapHistory;
import com.noleme.console.context.history.task.TaskHistory;
import com.noleme.console.output.ConsoleOutput;
import com.noleme.console.output.Output;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 18/12/2019
 */
public class Context
{
    private History history;
    private Output output = new ConsoleOutput();
    private boolean includeContext = false;

    /**
     *
     */
    public Context()
    {
        this(MapHistory::new);
    }

    /**
     *
     * @param provider
     */
    public Context(HistoryProvider provider)
    {
        this(provider.provide());
    }

    /**
     *
     * @param history
     */
    public Context(History history)
    {
        this.history = history;
    }

    /**
     * Creates a new context that will share reference data with a parent context (eg. task execution statuses).
     * You can then modify the context settings without altering the parent.
     *
     * @param parent
     * @return
     */
    public static Context inherit(Context parent)
    {
        return new Context(parent.history);
    }

    /**
     *
     * @param parent
     * @return
     */
    public Context inheritFrom(Context parent)
    {
        this.history = parent.history;
        return this;
    }

    /**
     *
     * @param task
     * @return
     */
    public boolean hasHistory(String task)
    {
        return this.history.hasHistory(task);
    }

    /**
     *
     * @param task
     * @return
     */
    public TaskHistory history(String task)
    {
        return this.history.history(task);
    }

    public Context setOutput(Output output)
    {
        this.output = output;
        return this;
    }

    public Context setIncludeContext(boolean includeContext)
    {
        this.includeContext = includeContext;
        return this;
    }

    public Output getOutput()
    {
        return this.output;
    }

    public boolean doesIncludeContext()
    {
        return this.includeContext;
    }

    /**
     * Internal API.
     *
     * @return
     */
    public History getHistory()
    {
        return this.history;
    }
}
