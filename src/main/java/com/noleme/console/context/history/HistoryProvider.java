package com.noleme.console.context.history;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 01/01/2020
 */
public interface HistoryProvider
{
    /**
     *
     * @return
     */
    History provide();
}
