package com.noleme.console.context.history.task;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 01/01/2020
 */
public interface TaskHistoryProvider
{
    /**
     *
     * @param task
     * @return
     */
    TaskHistory provide(String task);
}
