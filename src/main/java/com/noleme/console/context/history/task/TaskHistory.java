package com.noleme.console.context.history.task;

import com.noleme.console.context.history.task.event.TaskStatus;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/12/2019
 */
public interface TaskHistory
{
    /**
     *
     * @param uid
     * @param status
     * @param args
     * @return
     */
    TaskHistory register(String uid, String[] args, TaskStatus status);

    /**
     *
     * @param uid
     * @param args
     * @param exception
     * @return
     */
    TaskHistory register(String uid, String[] args, Exception exception);
}
