package com.noleme.console.context.history.memory;

import com.noleme.console.context.history.History;
import com.noleme.console.context.history.task.TaskHistory;
import com.noleme.console.context.history.task.TaskHistoryProvider;
import com.noleme.console.context.history.task.memory.QueueTaskHistory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 01/01/2020
 */
public class MapHistory implements History
{
    private final TaskHistoryProvider provider;
    private final Map<String, TaskHistory> history;

    /**
     *
     */
    public MapHistory()
    {
        this((task) -> new QueueTaskHistory());
    }

    /**
     *
     * @param provider
     */
    public MapHistory(TaskHistoryProvider provider)
    {
        this.provider = provider;
        this.history = new ConcurrentHashMap<>();
    }

    @Override
    public boolean hasHistory(String task)
    {
        return this.history.containsKey(task);
    }

    @Override
    synchronized public TaskHistory history(String task)
    {
        if (!this.history.containsKey(task))
            this.history.put(task, this.provider.provide(task));
        return this.history.get(task);
    }

    /**
     *
     * @return
     */
    public Set<String> getTaskNames()
    {
        return this.history.keySet();
    }
}
