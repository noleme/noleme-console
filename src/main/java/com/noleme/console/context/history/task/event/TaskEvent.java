package com.noleme.console.context.history.task.event;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/12/2019
 */
public class TaskEvent
{
    private final String uid;
    private final TaskStatus status;
    private final String[] args;

    /**
     *
     * @param uid
     * @param args
     * @param status
     */
    public TaskEvent(String uid, String[] args, TaskStatus status)
    {
        this.uid = uid;
        this.args = args;
        this.status = status;
    }

    /**
     *
     * @param uid
     * @param args
     * @param status
     * @return
     */
    public static TaskEvent of(String uid, String[] args, TaskStatus status)
    {
        return new TaskEvent(uid, args, status);
    }

    public String getUid()
    {
        return this.uid;
    }

    public TaskStatus getStatus()
    {
        return this.status;
    }

    public String[] getArgs()
    {
        return this.args;
    }
}
