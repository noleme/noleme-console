package com.noleme.console.context.history;

import com.noleme.console.context.history.task.TaskHistory;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 01/01/2020
 */
public interface History
{
    /**
     *
     * @param task
     * @return
     */
    boolean hasHistory(String task);

    /**
     *
     * @param task
     * @return
     */
    TaskHistory history(String task);
}
