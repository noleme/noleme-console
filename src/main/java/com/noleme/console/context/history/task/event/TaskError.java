package com.noleme.console.context.history.task.event;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/12/2019
 */
public class TaskError extends TaskEvent
{
    private final Exception exception;

    /**
     *
     * @param uid
     * @param args
     * @param exception
     */
    public TaskError(String uid, String[] args, Exception exception)
    {
        super(uid, args, TaskStatus.INTERRUPTED);
        this.exception = exception;
    }

    /**
     *
     * @param uid
     * @param args
     * @param exception
     * @return
     */
    public static TaskError of(String uid, String[] args, Exception exception)
    {
        return new TaskError(uid, args, exception);
    }

    public Exception getException()
    {
        return this.exception;
    }
}
