package com.noleme.console.context.history.task.memory;

import com.noleme.commons.container.EvictingQueue;
import com.noleme.console.context.history.task.event.TaskError;
import com.noleme.console.context.history.task.event.TaskEvent;
import com.noleme.console.context.history.task.TaskHistory;
import com.noleme.console.context.history.task.event.TaskStatus;
import com.noleme.console.context.history.task.event.Timestamped;

import java.util.Queue;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 01/01/2020
 */
public class QueueTaskHistory implements TaskHistory
{
    private final EvictingQueue<Timestamped<TaskEvent>> eventStack;
    private final EvictingQueue<Timestamped<TaskError>> errorStack;
    private static final int DEFAULT_MAX_EVENTS = 50;

    public QueueTaskHistory()
    {
        this(DEFAULT_MAX_EVENTS);
    }

    /**
     *
     * @param maxEvents
     */
    public QueueTaskHistory(int maxEvents)
    {
        this.eventStack = new EvictingQueue<>(maxEvents);
        this.errorStack = new EvictingQueue<>(maxEvents);
    }

    @Override
    public TaskHistory register(String uid, String[] args, TaskStatus status)
    {
        this.eventStack.add(new Timestamped<>(new TaskEvent(uid, args, status)));
        return this;
    }

    @Override
    public TaskHistory register(String uid, String[] args, Exception exception)
    {
        TaskError error = TaskError.of(uid, args, exception);
        this.eventStack.add(new Timestamped<>(error));
        this.errorStack.add(new Timestamped<>(error));
        return this;
    }

    /**
     *
     * @return
     */
    public Queue<Timestamped<TaskEvent>> events()
    {
        return this.eventStack;
    }

    /**
     *
     * @return
     */
    public Queue<Timestamped<TaskError>> errors()
    {
        return this.errorStack;
    }
}
