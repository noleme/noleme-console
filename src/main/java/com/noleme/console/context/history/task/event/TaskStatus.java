package com.noleme.console.context.history.task.event;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/12/2019
 */
public enum TaskStatus
{
    INERT(false),
    STARTED(true),
    FINISHED(false),
    INTERRUPTED(false);

    private final boolean active;

    TaskStatus(boolean active)
    {
        this.active = active;
    }

    public boolean isActive()
    {
        return this.active;
    }
}
