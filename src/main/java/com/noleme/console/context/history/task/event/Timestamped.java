package com.noleme.console.context.history.task.event;

import java.util.Date;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/12/2019
 */
public final class Timestamped<T>
{
    private final T data;
    private final Date timestamp;

    /**
     *
     * @param data
     */
    public Timestamped(T data)
    {
        this.data = data;
        this.timestamp = new Date();
    }

    public T data()
    {
        return this.data;
    }

    public Date timestamp()
    {
        return this.timestamp;
    }
}
