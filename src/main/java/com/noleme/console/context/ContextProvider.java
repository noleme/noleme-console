package com.noleme.console.context;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 20/12/2019
 */
public interface ContextProvider
{
    /**
     *
     * @return
     */
    Context provide();

    /**
     *
     * @param context
     * @return
     */
    static ContextProvider provide(Context context)
    {
        return () -> context;
    }
}
