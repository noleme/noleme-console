package com.noleme.console.loop;

import java.util.concurrent.*;
import java.util.concurrent.TimeUnit;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 25/10/2018
 */
public class TaskLauncher
{
    private final ExecutorService pool;
    private final BlockingQueue<Runnable> executionQueue;
    private final int maxQueueSize;
    private boolean waitUponShutdown;

    public TaskLauncher(int poolSize, int maxQueueSize)
    {
        this.maxQueueSize = maxQueueSize;
        this.executionQueue = new ArrayBlockingQueue<>(this.maxQueueSize);
        this.pool = new ThreadPoolExecutor(poolSize, poolSize, 0L, TimeUnit.MILLISECONDS, this.executionQueue);
    }

    public Future<?> submit(Runnable runnable)
    {
        return this.pool.submit(runnable);
    }

    public boolean isWaitUponShutdown()
    {
        return this.waitUponShutdown;
    }

    public TaskLauncher setWaitUponShutdown(boolean waitUponShutdown)
    {
        this.waitUponShutdown = waitUponShutdown;
        return this;
    }

    public void shutdown()
    {
        this.pool.shutdown();
    }

    public void shutdownNow()
    {
        this.pool.shutdownNow();
    }

    public boolean isTerminated()
    {
        return this.pool.isTerminated();
    }

    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException
    {
        return this.pool.awaitTermination(timeout, unit);
    }

    public void waitForQueue() throws InterruptedException
    {
        while (this.executionQueue.size() >= this.maxQueueSize)
            Thread.sleep(10);
    }

    public void waitForEmptyQueue() throws InterruptedException
    {
        while (!this.executionQueue.isEmpty())
            Thread.sleep(10);
    }

    public void emptyQueue()
    {
        this.executionQueue.clear();
    }
}
