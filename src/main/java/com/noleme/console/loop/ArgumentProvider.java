package com.noleme.console.loop;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 17/04/2018
 */
public interface ArgumentProvider
{
    String[] provide();

    /**
     *
     * @return
     */
    static ArgumentProvider provideEmpty()
    {
        return () -> new String[0];
    }

    /**
     *
     * @param arguments
     * @return
     */
    static ArgumentProvider provide(String... arguments)
    {
        return () -> arguments;
    }

    /**
     *
     * @param arguments
     * @return
     */
    static String[] toArray(String... arguments)
    {
        return arguments;
    }
}
