package com.noleme.console.loop.async;

import com.noleme.console.Console;
import com.noleme.console.context.Context;
import com.noleme.console.context.ContextProvider;
import com.noleme.console.loop.Loop;
import com.noleme.console.loop.LoopException;
import com.noleme.console.loop.TaskLauncher;
import com.noleme.console.loop.task.LoopingTask;
import com.noleme.console.runtime.ConsoleRuntime;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 25/10/2018
 */
public class AsyncLoop extends Loop
{
    private final TaskLauncher launcher;
    private boolean stopRequested;

    /**
     *
     * @param runtime
     * @param context
     */
    public AsyncLoop(ConsoleRuntime runtime, Context context)
    {
        this(runtime, context, 16, 500, false);
    }

    /**
     *
     * @param runtime
     * @param context
     * @param poolSize
     * @param maxQueueSize
     * @param waitUponExit
     */
    public AsyncLoop(ConsoleRuntime runtime, Context context, int poolSize, int maxQueueSize, boolean waitUponExit)
    {
        super(runtime, context);
        this.launcher = new TaskLauncher(poolSize, maxQueueSize).setWaitUponShutdown(waitUponExit);
    }

    @Override
    protected void submit(LoopingTask task) throws LoopException
    {
        try {
            this.launcher.waitForQueue();

            this.launcher.submit(task.getRunnable(
                this.runtime,
                task.getContextProvider() != null
                    ? task.getContextProvider()
                    : ContextProvider.provide(this.context)
            ));
        }
        catch (InterruptedException e) {
            throw new LoopException("An unexpected error occurred.", e);
        }
    }

    @Override
    protected boolean loopStart()
    {
        return !this.stopRequested;
    }

    @Override
    synchronized protected void shutdown()
    {
        super.shutdown();
        try {
            this.stopRequested = true;
            if (this.launcher.isWaitUponShutdown())
                this.launcher.waitForEmptyQueue();
            else
                this.launcher.emptyQueue();

            this.launcher.shutdown();
            this.launcher.awaitTermination(15, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
            this.context.getOutput().block.error(e.getMessage());
        }
        finally {
            if (!this.launcher.isTerminated())
                this.launcher.shutdownNow();
            this.context.getOutput().block.info("Loop shutdown complete ("+(new Date())+")...");
        }
    }
}
