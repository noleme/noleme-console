package com.noleme.console.loop;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 17/04/2018
 */
public class LoopException extends Exception
{
    /**
     *
     * @param message
     */
    public LoopException(String message)
    {
        super(message);
    }

    /**
     *
     * @param message
     * @param parent
     */
    public LoopException(String message, Throwable parent)
    {
        super(message, parent);
    }
}
