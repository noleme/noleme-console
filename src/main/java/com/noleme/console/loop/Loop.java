package com.noleme.console.loop;

import com.noleme.console.context.Context;
import com.noleme.console.loop.task.ConsumableTask;
import com.noleme.console.loop.task.LoopingTask;
import com.noleme.console.loop.task.PlannedTask;
import com.noleme.console.runtime.ConsoleRuntime;

import java.util.Date;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 17/04/2018
 */
public abstract class Loop implements ConsoleRuntime
{
    /** In seconds */
    private final static long TICK_SIZE = 1;
    protected final ConsoleRuntime runtime;
    protected final Context context;
    private final Queue<LoopingTask> tasks;
    private Status status;
    private long tick;

    /**
     *
     * @param runtime
     * @param context
     */
    public Loop(ConsoleRuntime runtime, Context context)
    {
        this.runtime = runtime;
        this.context = context;
        this.tick = 0;
        this.status = Status.UP;
        this.tasks = new ConcurrentLinkedQueue<>();
    }

    /**
     *
     * @throws LoopException
     */
    public void start() throws LoopException
    {
        try {
            Runtime.getRuntime().addShutdownHook(new ShutdownHook());

            while (true)
            {
                if (!this.loopStart())
                    break;

                if (this.status == Status.UP)
                {
                    Iterator<LoopingTask> it = this.tasks.iterator();
                    while (it.hasNext())
                    {
                        LoopingTask task = it.next();

                        long actualTick = this.tick - task.getDelay();
                        //In order to actually be able to handle TICK_SIZE > 1, we need to make sure the task tick match the loop ticks (or they'll never align) ; the following should do the trick, might enable it someday.
                        //long taskTick = task.getTick() + (TICK_SIZE - (task.getTick() % TICK_SIZE));
                        if (actualTick < 0)
                            continue;
                        if (actualTick % task.getTick() != 0)
                            continue;

                        this.submit(task);

                        if (task instanceof ConsumableTask && ((ConsumableTask) task).consume() <= 0)
                        {
                            this.unregister(task);
                            it.remove();
                        }
                    }
                }

                Thread.sleep(TICK_SIZE * 1000);
                this.tick += TICK_SIZE;

                if (!this.loopEnd())
                    break;
            }
        }
        catch (InterruptedException e) {
            throw new LoopException("An unexpected error occurred.", e);
        }
    }

    /**
     *
     * @return
     * @throws LoopException
     */
    protected boolean loopStart() throws LoopException
    {
        return true;
    }

    /**
     *
     * @return
     * @throws LoopException
     */
    protected boolean loopEnd() throws LoopException
    {
        return true;
    }

    /**
     *
     * @param task
     * @throws LoopException
     */
    protected abstract void submit(LoopingTask task) throws LoopException;

    @Override
    public void run(PlannedTask task)
    {
        /* If the provided task is not a LoopingTask subclass, we create a ConsumableTask from its components */
        if (!(task instanceof LoopingTask))
        {
            ConsumableTask consumable = new ConsumableTask(
                task.getTask(),
                task.getArgumentProvider(),
                1,
                0
            );
            consumable
                .setContextProvider(task.getContextProvider())
                .setRuntimeProvider(task.getRuntimeProvider())
            ;
            task = consumable;
        }

        this.register((LoopingTask) task);
    }

    /**
     *
     * @param task
     * @return
     */
    public Loop register(LoopingTask task)
    {
        synchronized (this)
        {
            this.tasks.add(task);
            return this;
        }
    }

    /**
     * Handle unregistration actions here
     *
     * @param task
     */
    protected void unregister(PlannedTask task)
    {

    }

    public final void stop()
    {
        synchronized (this)
        {
            if (this.status == Status.DOWN)
                return;
            this.status = Status.DOWN;
        }
    }

    public final void resume()
    {
        synchronized (this)
        {
            if (this.status == Status.UP)
                return;
            this.status = Status.UP;
        }
    }

    public final Status getStatus()
    {
        return this.status;
    }

    /**
     *
     */
    protected void shutdown()
    {
        this.context.getOutput().block.info("Loop shutdown started ("+(new Date())+")...");
    }

    /**
     *
     */
    private class ShutdownHook extends Thread
    {
        @Override public void run()
        {
            shutdown();
        }
    }

    public enum Status
    {
        UP, DOWN,
    }
}
