package com.noleme.console.loop.task;

import com.noleme.console.loop.ArgumentProvider;
import com.noleme.console.loop.TimeUnit;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 18/12/2019
 */
public class ConsumableTask extends LoopingTask
{
    private int tickToLive;

    /**
     *
     * @param task
     * @param provider
     * @param ttl
     */
    public ConsumableTask(String task, ArgumentProvider provider, int ttl)
    {
        this(task, provider, ttl, 1);
    }

    /**
     *
     * @param task
     * @param provider
     * @param ttl
     * @param tick
     */
    public ConsumableTask(String task, ArgumentProvider provider, int ttl, long tick)
    {
        this(task, provider, ttl, tick, TimeUnit.SECONDS);
    }

    /**
     *
     * @param task
     * @param provider
     * @param ttl
     * @param tick
     * @param unit
     */
    public ConsumableTask(String task, ArgumentProvider provider, int ttl, long tick, TimeUnit unit)
    {
        super(task, provider, tick, unit);
        this.tickToLive = ttl;
    }

    /**
     *
     * @return
     */
    synchronized public int consume()
    {
        return --this.tickToLive;
    }
}
