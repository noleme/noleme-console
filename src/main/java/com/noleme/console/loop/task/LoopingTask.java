package com.noleme.console.loop.task;

import com.noleme.console.context.Context;
import com.noleme.console.context.ContextProvider;
import com.noleme.console.exception.ConsoleException;
import com.noleme.console.loop.ArgumentProvider;
import com.noleme.console.loop.TimeUnit;
import com.noleme.console.runtime.ConsoleRuntime;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 24/10/2018
 */
public class LoopingTask extends PlannedTask
{
    private final long tick;
    private long delay;

    /**
     *
     * @param task
     * @param provider
     * @param tick
     */
    public LoopingTask(String task, ArgumentProvider provider, long tick)
    {
        this(task, provider, tick, TimeUnit.SECONDS);
    }

    /**
     *
     * @param task
     * @param provider
     * @param tick
     * @param unit
     */
    public LoopingTask(String task, ArgumentProvider provider, long tick, TimeUnit unit)
    {
        super(task, provider);

        if (tick == 0)
            tick = 1;

        this.tick = unit.toSeconds(tick);
        this.delay = 0;
    }

    /**
     *
     * @param runtime
     * @return
     */
    public Runnable getRunnable(ConsoleRuntime runtime, ContextProvider contextProvider)
    {
        return () -> {
            Context context = contextProvider.provide();

            try {
                runtime.run(
                    this.getTask(),
                    context,
                    this.getArgumentProvider().provide()
                );
            }
            catch (ConsoleException e) {
                StringBuilder sb = new StringBuilder(e.getMessage());

                Throwable cause = e.getCause();
                while (cause != null)
                {
                    sb.append("\n").append("-- ").append(cause.getClass().getName()).append(": ").append((cause.getMessage() != null) ? cause.getMessage() : "<no message>");

                    for (StackTraceElement el : cause.getStackTrace())
                        sb.append("\n").append("   - at ").append(el.getClassName()).append(".").append(el.getMethodName()).append(" l:").append(el.getLineNumber());

                    cause = cause.getCause();
                }

                context.getOutput().line.error(sb.toString());
            }
        };
    }

    public long getTick()
    {
        return this.tick;
    }

    public long getDelay()
    {
        return this.delay;
    }

    /**
     *
     * @param delay
     * @param unit
     * @return
     */
    public LoopingTask setDelay(long delay, TimeUnit unit)
    {
        this.delay = unit.toSeconds(delay);
        return this;
    }
}
