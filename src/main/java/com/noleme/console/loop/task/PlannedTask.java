package com.noleme.console.loop.task;

import com.noleme.console.context.ContextProvider;
import com.noleme.console.loop.ArgumentProvider;
import com.noleme.console.runtime.RuntimeProvider;

import java.util.UUID;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 19/12/2019
 */
public class PlannedTask
{
    private final String uid;
    private final String task;
    private final ArgumentProvider argumentProvider;
    private ContextProvider contextProvider;
    private RuntimeProvider runtimeProvider;

    /**
     *
     * @param task
     * @param provider
     */
    public PlannedTask(String task, ArgumentProvider provider)
    {
        this.uid = UUID.randomUUID().toString();
        this.task = task;
        this.argumentProvider = provider;
    }

    /**
     *
     * @return
     */
    public String getTask()
    {
        return this.task;
    }

    /**
     *
     * @return
     */
    public ArgumentProvider getArgumentProvider()
    {
        return this.argumentProvider;
    }

    /**
     *
     * @return
     */
    public ContextProvider getContextProvider()
    {
        return this.contextProvider;
    }

    /**
     *
     * @return
     */
    public RuntimeProvider getRuntimeProvider()
    {
        return this.runtimeProvider;
    }

    /**
     *
     * @return
     */
    public String getUid()
    {
        return this.uid;
    }

    /**
     *
     * @param contextProvider
     * @return
     */
    public PlannedTask setContextProvider(ContextProvider contextProvider)
    {
        this.contextProvider = contextProvider;
        return this;
    }

    /**
     *
     * @param runtimeProvider
     * @return
     */
    public PlannedTask setRuntimeProvider(RuntimeProvider runtimeProvider)
    {
        this.runtimeProvider = runtimeProvider;
        return this;
    }
}
