package com.noleme.console.loop.sync;

import com.noleme.console.context.Context;
import com.noleme.console.context.ContextProvider;
import com.noleme.console.loop.Loop;
import com.noleme.console.loop.LoopException;
import com.noleme.console.loop.TaskLauncher;
import com.noleme.console.loop.task.LoopingTask;
import com.noleme.console.loop.task.PlannedTask;
import com.noleme.console.runtime.ConsoleRuntime;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 25/10/2018
 */
public class SyncLoop extends Loop
{
    private final Map<String, TaskLauncher> launchers;
    private boolean stopRequested;

    /**
     *
     * @param runtime
     * @param context
     */
    public SyncLoop(ConsoleRuntime runtime, Context context)
    {
        super(runtime, context);
        this.launchers = new HashMap<>();
        this.stopRequested = false;
    }

    @Override
    public Loop register(LoopingTask task)
    {
        return this.register(task, 1);
    }

    /**
     *
     * @param task
     * @param concurrency
     * @return
     */
    public Loop register(LoopingTask task, int concurrency)
    {
        return this.register(task, concurrency, 500, false);
    }

    /**
     *
     * @param task
     * @param concurrency
     * @param maxQueueSize
     * @return
     */
    public Loop register(LoopingTask task, int concurrency, int maxQueueSize, boolean waitUponShutdown)
    {
        super.register(task);
        this.launchers.put(task.getUid(), new TaskLauncher(concurrency, maxQueueSize).setWaitUponShutdown(waitUponShutdown));
        return this;
    }

    @Override
    protected void unregister(PlannedTask task)
    {
        super.unregister(task);
        this.launchers.remove(task.getUid());
    }

    @Override
    protected boolean loopStart()
    {
        return !this.stopRequested;
    }

    @Override
    protected void submit(LoopingTask task) throws LoopException
    {
        try {
            TaskLauncher launcher = this.launchers.get(task.getUid());
            launcher.waitForQueue();

            launcher.submit(task.getRunnable(
                this.runtime,
                task.getContextProvider() != null
                    ? task.getContextProvider()
                    : ContextProvider.provide(this.context)
            ));
        }
        catch (InterruptedException e) {
            throw new LoopException("An unexpected error occurred.", e);
        }
    }

    @Override
    protected void shutdown()
    {
        super.shutdown();
        try {
            this.stopRequested = true;
            for (TaskLauncher launcher : this.launchers.values())
            {
                if (launcher.isWaitUponShutdown())
                   launcher.waitForEmptyQueue();
                else
                    launcher.emptyQueue();
            }

            for (TaskLauncher launcher : this.launchers.values())
                launcher.shutdown();
            for (TaskLauncher launcher : this.launchers.values())
                launcher.awaitTermination(15, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
            this.context.getOutput().block.error(e.getMessage());
        }
        finally {
            for (TaskLauncher launcher : this.launchers.values())
            {
                if (!launcher.isTerminated())
                    launcher.shutdownNow();
            }
            this.context.getOutput().block.info("Loop shutdown complete ("+(new Date())+")...");
        }
    }

    /**
     *
     * @return
     */
    protected final Map<String, TaskLauncher> getLaunchers()
    {
        return this.launchers;
    }

    /**
     *
     * @return
     */
    protected final boolean isStopRequested()
    {
        return this.stopRequested;
    }
}
