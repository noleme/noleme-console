package com.noleme.console.loop;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 24/10/2018
 */
public enum TimeUnit
{
    SECONDS(1),
    MINUTES(60),
    HOURS(3600),
    DAYS(86400);

    private long factor;

    /**
     *
     * @param factor
     */
    TimeUnit(long factor)
    {
        this.factor = factor;
    }

    /**
     *
     * @param timespan
     * @return
     */
    public long toSeconds(long timespan)
    {
        return timespan * factor;
    }
}
