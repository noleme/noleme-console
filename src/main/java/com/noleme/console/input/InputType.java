package com.noleme.console.input;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public enum InputType
{
    REQUIRED,
    OPTIONAL;
}
