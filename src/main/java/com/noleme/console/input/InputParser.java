package com.noleme.console.input;

import com.noleme.console.exception.ConsoleException;
import com.noleme.console.task.description.ArgumentDescription;
import com.noleme.console.task.description.InputDescription;
import com.noleme.console.task.description.ParameterDescription;
import com.noleme.console.task.description.TaskDescription;
import com.noleme.console.task.description.validator.InputValidator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class InputParser
{
    /**
     *
     * @param args
     * @param description
     * @return
     * @throws ConsoleException
     */
    public Input parse(String[] args, TaskDescription description) throws ConsoleException
    {
        Input input = new Input();

        if (description != null)
        {
            ArgumentDescription lastDescription = null;

            int argIndex = 0;
            /* Handling defined entries */
            for (int i = 0 ; i < args.length ; ++i)
            {
                String arg = args[i];

                if (arg.startsWith("--"))
                {
                    String param = arg.substring(2);
                    String value = null;

                    if ((i + 1) < args.length && !args[i + 1].startsWith("--"))
                    {
                        value = args[i + 1];
                        ++i;
                    }

                    if (!description.getParameters().containsKey(param))
                    {
                        if (!description.undeclaredParametersAllowed())
                            throw new InputParserException("Unknown "+param+" parameters.");

                        input.undeclaredParameters.set(param, value);
                    }
                    else
                        this.handleParameter(param, value, description, input.parameters);
                }
                else {
                    lastDescription = this.handleArgument(argIndex++, arg, description, lastDescription, input.arguments);
                }
            }

            /* Handling undefined entries */
            for (InputDescription desc : description.getArguments())
            {
                if (!input.arguments.has(desc.getName()))
                {
                    if (desc.getType() == InputType.REQUIRED)
                        throw new InputParserException("A required \""+desc.getName()+"\" argument is missing.");
                    else if (desc.getDefaultValue() != null)
                        this.produceInput(desc.getDefaultValue(), desc, input.arguments);
                }
            }
            for (Map.Entry<String, ParameterDescription> descEntry : description.getParameters().entrySet())
            {
                ParameterDescription desc = descEntry.getValue();
                if (!input.parameters.has(desc.getName()))
                {
                    if (desc.getType() == InputType.REQUIRED)
                        throw new InputParserException("A required \""+descEntry.getKey()+"\" parameter is missing.");
                    else if (desc.getDefaultValue() != null)
                        this.produceInput(desc.getDefaultValue(), desc, input.parameters);
                }
            }
        }

        return input;
    }

    /**
     *
     * @param param
     * @param value
     * @param task
     * @param parameters
     * @throws ConsoleException
     */
    private void handleParameter(String param, String value, TaskDescription task, InputHolder parameters) throws ConsoleException
    {
        ParameterDescription description = task.getParameters().get(param);

        if (parameters.has(description.getName()) && !description.isVariable())
            throw new InputParserException("Found a duplicate "+param+" parameter, parameters can only be passed once.");

        this.produceInput(value, description, parameters);
    }

    /**
     *
     * @param index
     * @param value
     * @param task
     * @param lastDescription
     * @param arguments
     * @return
     * @throws ConsoleException
     */
    private ArgumentDescription handleArgument(int index, String value, TaskDescription task, ArgumentDescription lastDescription, InputHolder arguments) throws ConsoleException
    {
        if (task.getArguments().size() < (index + 1))
        {
            if (lastDescription != null && lastDescription.isVariable())
            {
                this.produceInput(value, lastDescription, arguments);
                return lastDescription;
            }

            throw new InputParserException("Argument \""+value+"\" at index "+index+" does not correspond to any expected argument.");
        }

        ArgumentDescription description = task.getArguments().get(index);

        this.produceInput(value, description, arguments);

        return description;
    }

    /**
     *
     * @param value
     * @param description
     * @param inputs
     * @throws ConsoleException
     */
    private void produceInput(String value, InputDescription description, InputHolder inputs) throws ConsoleException
    {
        /* These are here in order to allow "--flag" type parameters with no specified values, which are interpreted as meaning "--flag true" */
        Object processed = (value == null ? true : value);
        value = (value == null ? "true" : value);

        if (description.getPreparator() != null)
            processed = description.getPreparator().prepare(value);
        for (InputValidator validator : description.getValidators())
        {
            if (!validator.validate(processed))
                throw new InputParserException("The provided value for "+description.getName()+" doesn't satisfy the specified requirements.");
        }

        if (description.isVariable())
        {
            if (!inputs.has(description.getName()))
                inputs.set(description.getName(), new ArrayList<>());
            ((Collection)inputs.get(description.getName())).add(processed);
        }
        else
            inputs.set(description.getName(), processed);
    }
}
