package com.noleme.console.input;

import com.noleme.console.exception.ConsoleException;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class InputParserException extends ConsoleException
{
    /**
     * @param message
     */
    public InputParserException(String message)
    {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public InputParserException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
