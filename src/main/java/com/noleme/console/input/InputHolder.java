package com.noleme.console.input;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class InputHolder
{
    private final Map<String, Object> properties;

    public InputHolder()
    {
        this.properties = new HashMap<>();
    }

    /**
     *
     * @param param
     * @return
     */
    public boolean has(String param)
    {
        return this.properties.containsKey(param);
    }

    /**
     *
     * @param param
     * @return
     */
    public Object get(String param)
    {
        return this.properties.get(param);
    }

    /**
     *
     * @return
     */
    public Map<String, Object> getProperties()
    {
        return this.properties;
    }

    /**
     *
     * @param param
     * @return
     */
    public Integer getInteger(String param)
    {
        return (Integer)this.properties.get(param);
    }

    /**
     *
     * @param param
     * @return
     */
    public Long getLong(String param)
    {
        return (Long)this.properties.get(param);
    }

    /**
     *
     * @param param
     * @return
     */
    public Double getDouble(String param)
    {
        return (Double)this.properties.get(param);
    }

    /**
     *
     * @param param
     * @return
     */
    public String getString(String param)
    {
        return (String)this.properties.get(param);
    }

    /**
     *
     * @param param
     * @return
     */
    public Boolean getBoolean(String param)
    {
        return (Boolean)this.properties.get(param);
    }

    /**
     *
     * @param param
     * @return
     */
    @SuppressWarnings("unchecked")
    public <E extends Enum> E getEnum(String param, Class<E> type)
    {
        return (E)this.properties.get(param);
    }

    /**
     *
     * @param param
     * @return
     */
    public Collection getCollection(String param)
    {
        return (Collection)this.properties.get(param);
    }

    /**
     *
     * @param param
     * @param value
     */
    public void set(String param, Object value)
    {
        this.properties.put(param, value);
    }
}
