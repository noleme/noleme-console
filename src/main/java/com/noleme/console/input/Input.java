package com.noleme.console.input;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class Input
{
    public final InputHolder arguments = new InputHolder();
    public final InputHolder parameters = new InputHolder();
    public final InputHolder undeclaredParameters = new InputHolder();
}
