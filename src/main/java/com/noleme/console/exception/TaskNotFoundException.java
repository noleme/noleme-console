package com.noleme.console.exception;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 29/04/2017
 */
public class TaskNotFoundException extends ConsoleException
{
    public TaskNotFoundException(String message)
    {
        super(message);
    }
}
