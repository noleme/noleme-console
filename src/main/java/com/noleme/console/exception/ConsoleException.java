package com.noleme.console.exception;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 29/04/2017
 */
public class ConsoleException extends Exception
{
    /**
     *
     * @param message
     */
    public ConsoleException(String message)
    {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public ConsoleException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
