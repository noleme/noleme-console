package com.noleme.console.task;

import com.noleme.console.Console;
import com.noleme.console.input.Input;
import com.noleme.console.output.Output;
import com.noleme.console.output.style.ConsoleColor;
import com.noleme.console.task.description.TaskDescription;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 29/04/2017
 */
public class List implements Task
{
    @Override
    public TaskDescription setup()
    {
        return new TaskDescription().setDescription("Lists all defined tasks.");
    }

    @Override
    public void run(Console console, Input input, Output output)
    {
        Set<String> taskNames = console.listTasks();
        java.util.List<String> sortedTasks = new ArrayList<>(taskNames);
        Collections.sort(sortedTasks);

        this.printOutput(console, output, sortedTasks);
    }

    /**
     *
     * @param console
     * @param sortedTasks
     * @return
     */
    private void printOutput(Console console, Output output, java.util.List<String> sortedTasks)
    {
        int namePadding = this.calculateNamePadding(console);

        for (String taskName : sortedTasks)
        {
            Task task = console.getTask(taskName);
            TaskDescription description = console.getTaskDescription(taskName);

            output.print(output.color.wrap(taskName, ConsoleColor.CYAN_BRIGHT));

            /* Handle name padding */
            if (taskName.length() < namePadding)
            {
                for (int i = 0 ; i < (namePadding - taskName.length()) ; ++i)
                    output.print(" ");
            }

            /* Handle deprecated mentions */
            Deprecated deprecated = task.getClass().getAnnotation(Deprecated.class);
            if (deprecated != null)
                output.print(output.color.wrap("[deprecated] ", ConsoleColor.PURPLE));

            /* Handle descriptions */
            output.println(description != null ? description.getDescription() : "~");
        }
    }

    /**
     *
     * @param console
     * @return
     */
    private int calculateNamePadding(Console console)
    {
        Set<String> taskNames = console.listTasks();

        int padding = 0;

        for (String taskName : taskNames)
        {
            if (padding < taskName.length())
                padding = taskName.length();
        }

        return padding + 10;
    }
}
