package com.noleme.console.task;

import com.noleme.console.Console;
import com.noleme.console.input.Input;
import com.noleme.console.input.InputType;
import com.noleme.console.output.Output;
import com.noleme.console.output.style.ConsoleColor;
import com.noleme.console.task.description.ArgumentDescription;
import com.noleme.console.task.description.InputDescription;
import com.noleme.console.task.description.ParameterDescription;
import com.noleme.console.task.description.TaskDescription;
import com.noleme.console.task.exception.TaskException;
import org.apache.commons.text.StringEscapeUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 04/12/2017
 */
public class Help implements Task
{
    /**
     * @return
     */
    @Override
    public TaskDescription setup()
    {
        return new TaskDescription()
            .setDescription("Shows details about a task.")
            .addArgument("task_name", InputType.REQUIRED)
        ;
    }

    /**
     * @param console
     * @param input
     * @throws TaskException
     */
    @Override
    public void run(Console console, Input input, Output output) throws TaskException
    {
        String taskName = input.arguments.getString("task_name");

        if (!console.hasTask(taskName))
            throw new TaskException("No task named '"+taskName+"' has been defined on this console.");

        Task task = console.getTask(taskName);
        TaskDescription description = console.getTaskDescription(taskName);

        /* Handle deprecated mentions */
        Deprecated deprecated = task.getClass().getAnnotation(Deprecated.class);
        if (deprecated != null)
            output.line.custom(output.color.wrap("Deprecated:", ConsoleColor.PURPLE)+" This task is deprecated.", ConsoleColor.PURPLE);

        output.line.info(output.color.wrap("Name:", ConsoleColor.CYAN)+" "+taskName);
        output.line.info(output.color.wrap("Description:", ConsoleColor.CYAN)+" "+(description != null ? description.getDescription() : "~"));
        output.line.info(output.color.wrap("Usage:", ConsoleColor.CYAN));

        /* Prepare task full name */
        String name = this.buildName(taskName);
        String argsDescription = this.buildArguments(description);

        output.print("\n\t");
        output.print(output.color.wrap(name, ConsoleColor.CYAN_BRIGHT));
        if (!argsDescription.isEmpty())
            output.print(" "+output.color.wrap(argsDescription, ConsoleColor.YELLOW));
        output.println();
    }

    /**
     *
     * @param taskName
     */
    private String buildName(String taskName)
    {
        return taskName;
    }

    /**
     *
     * @param description
     * @return
     */
    private String buildArguments(TaskDescription description)
    {
        if (description == null)
            return "";

        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (ArgumentDescription desc : description.getArguments())
        {
            if (i > 0)
                sb.append(" ");
            sb.append("{").append(this.buildInputName(desc)).append("}");
            ++i;
        }
        java.util.List<Map.Entry<String, ParameterDescription>> parameterList = new ArrayList<>();
        parameterList.addAll(description.getParameters().entrySet());
        parameterList.sort(Comparator.comparing(Map.Entry::getKey));

        for (Map.Entry<String, ParameterDescription> desc : parameterList)
        {
            if (i > 0)
                sb.append(" ");
            sb.append("--").append(desc.getKey()).append(" {").append(this.buildInputName(desc.getValue())).append("}");
            ++i;
        }
        return sb.toString();
    }

    /**
     *
     * @param description
     * @return
     */
    private String buildInputName(InputDescription description)
    {
        return description.getName()
            +(description.getType() == InputType.OPTIONAL ? "?" : "")
            +(description.getDefaultValue() != null ? "["+escapeString(description.getDefaultValue())+"]" : "")
            +(description.isVariable() ? " ..." : "")
        ;
    }

    /**
     *
     * @param string
     * @return
     */
    private String escapeString(String string)
    {
        return StringEscapeUtils.escapeJava(string);
    }
}
