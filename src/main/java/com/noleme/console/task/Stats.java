package com.noleme.console.task;

import com.noleme.console.Console;
import com.noleme.console.input.Input;
import com.noleme.console.output.Output;
import com.noleme.console.task.description.TaskDescription;
import com.noleme.console.task.exception.TaskException;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 12/03/2022
 */
public class Stats implements Task
{
    @Override
    public TaskDescription setup()
    {
        return new TaskDescription().setDescription("Lists JVM settings.");
    }

    @Override
    public void run(Console console, Input input, Output output) throws TaskException
    {
        int mb = 1024 * 1024;
        MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();

        long xmx = memoryBean.getHeapMemoryUsage().getMax() / mb;
        long xms = memoryBean.getHeapMemoryUsage().getInit() / mb;

        output.line.info("Initial Memory (xms) : " + xms + "MB");
        output.line.info("Initial Memory (xmx) : " + xmx + "MB");
    }
}
