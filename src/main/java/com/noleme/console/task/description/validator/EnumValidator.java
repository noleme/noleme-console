package com.noleme.console.task.description.validator;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class EnumValidator <E extends Enum> implements InputValidator<E>
{
    private E[] possibleValues;

    /**
     *
     * @param possibleValues
     */
    public EnumValidator(E... possibleValues)
    {
        this.possibleValues = possibleValues;
    }

    /**
     * @param input
     * @return
     * @throws InvalidInputException
     */
    @Override
    public boolean validate(E input) throws InvalidInputException
    {
        for (E possibleValue : this.possibleValues)
        {
            if (input == possibleValue)
                return true;
        }
        return false;
    }
}
