package com.noleme.console.task.description.preparator;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public interface InputPreparator
{
    /**
     *
     * @param input
     * @return
     * @throws InputPreparationException
     */
    Object prepare(String input) throws InputPreparationException;
}
