package com.noleme.console.task.description.preparator;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 27/03/2018
 */
public class DoublePreparator implements InputPreparator
{
    /**
     * @param input
     * @return
     * @throws InputPreparationException
     */
    @Override
    public Object prepare(String input) throws InputPreparationException
    {
        return Double.parseDouble(input);
    }
}
