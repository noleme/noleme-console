package com.noleme.console.task.description.preparator;

import com.noleme.console.exception.ConsoleException;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class InputPreparationException extends ConsoleException
{
    /**
     * @param message
     */
    public InputPreparationException(String message)
    {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public InputPreparationException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
