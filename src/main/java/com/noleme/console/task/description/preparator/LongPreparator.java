package com.noleme.console.task.description.preparator;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 27/03/2018
 */
public class LongPreparator implements InputPreparator
{
    /**
     * @param input
     * @return
     * @throws InputPreparationException
     */
    @Override
    public Object prepare(String input) throws InputPreparationException
    {
        return Long.parseLong(input);
    }
}
