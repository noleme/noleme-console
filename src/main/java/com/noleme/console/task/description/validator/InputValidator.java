package com.noleme.console.task.description.validator;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public interface InputValidator <T>
{
    /**
     *
     * @param input
     * @return
     * @throws InvalidInputException
     */
    boolean validate(T input) throws InvalidInputException;
}
