package com.noleme.console.task.description.validator;

import com.noleme.console.exception.ConsoleException;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class InvalidInputException extends ConsoleException
{
    /**
     *
     * @param message
     */
    public InvalidInputException(String message)
    {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public InvalidInputException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
