package com.noleme.console.task.description;

import com.noleme.console.input.InputType;
import com.noleme.console.task.description.preparator.InputPreparator;
import com.noleme.console.task.description.validator.InputValidator;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public abstract class InputDescription <S extends InputDescription>
{
    private final String name;
    private final InputType type;
    private String defaultValue;
    private boolean variable;
    private InputPreparator preparator;
    private InputValidator[] validators;

    /**
     *
     * @param type
     */
    public InputDescription(String name, InputType type)
    {
        this.name = name;
        this.type = type;
        this.variable = false;
        this.validators = new InputValidator[]{};
    }

    /**
     *
     * @return
     */
    public String getName()
    {
        return this.name;
    }

    /**
     *
     * @return
     */
    public InputType getType()
    {
        return this.type;
    }

    /**
     *
     * @return
     */
    public String getDefaultValue()
    {
        return this.defaultValue;
    }

    /**
     *
     * @return
     */
    public boolean isVariable()
    {
        return this.variable;
    }

    /**
     *
     * @return
     */
    public InputPreparator getPreparator()
    {
        return this.preparator;
    }

    /**
     *
     * @return
     */
    public InputValidator[] getValidators()
    {
        return this.validators;
    }

    /**
     *
     * @param defaultValue
     * @return
     */
    @SuppressWarnings("unchecked")
    public S setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
        return (S)this;
    }

    /**
     *
     * @param variable
     * @return
     */
    @SuppressWarnings("unchecked")
    public S setVariable(boolean variable)
    {
        this.variable = variable;
        return (S)this;
    }


    /**
     *
     * @param preparator
     * @return
     */
    @SuppressWarnings("unchecked")
    public S setPreparator(InputPreparator preparator)
    {
        this.preparator = preparator;
        return (S)this;
    }

    /**
     *
     * @param validators
     * @return
     */
    @SuppressWarnings("unchecked")
    public S setValidator(InputValidator... validators)
    {
        this.validators = validators;
        return (S)this;
    }
}
