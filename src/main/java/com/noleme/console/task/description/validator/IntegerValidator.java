package com.noleme.console.task.description.validator;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class IntegerValidator
{
    private static abstract class Comparison implements InputValidator<Integer>
    {
        protected int comparison;

        public Comparison(int comparison)
        {
            this.comparison = comparison;
        }
    }

    public static class Equal extends Comparison
    {
        public Equal(int comparison) { super(comparison); }

        @Override
        public boolean validate(Integer input) throws InvalidInputException
        {
            return input == this.comparison;
        }
    }

    public static class Higher extends Comparison
    {
        public Higher(int comparison) { super(comparison); }

        @Override
        public boolean validate(Integer input) throws InvalidInputException
        {
            return input > this.comparison;
        }
    }

    public static class Lower extends Comparison
    {
        public Lower(int comparison) { super(comparison); }

        @Override
        public boolean validate(Integer input) throws InvalidInputException
        {
            return input < this.comparison;
        }
    }

    public static class HigherEqual extends Comparison
    {
        public HigherEqual(int comparison) { super(comparison); }

        @Override
        public boolean validate(Integer input) throws InvalidInputException
        {
            return input >= this.comparison;
        }
    }

    public static class LowerEqual extends Comparison
    {
        public LowerEqual(int comparison) { super(comparison); }

        @Override
        public boolean validate(Integer input) throws InvalidInputException
        {
            return input <= this.comparison;
        }
    }
}
