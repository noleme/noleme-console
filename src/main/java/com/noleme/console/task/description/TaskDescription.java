package com.noleme.console.task.description;

import com.noleme.console.input.InputType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class TaskDescription
{
    private String description;
    private List<ArgumentDescription> arguments;
    private Map<String, ParameterDescription> parameters;
    private boolean allowUndeclaredParameters;

    /**
     *
     */
    public TaskDescription()
    {
        this.description = "~";
        this.arguments = new ArrayList<>();
        this.parameters = new HashMap<>();
        this.allowUndeclaredParameters = false;
    }

    /**
     *
     * @return
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     *
     * @return
     */
    public List<ArgumentDescription> getArguments()
    {
        return this.arguments;
    }

    /**
     *
     * @return
     */
    public Map<String, ParameterDescription> getParameters()
    {
        return this.parameters;
    }

    /**
     *
     * @return
     */
    public boolean undeclaredParametersAllowed()
    {
        return this.allowUndeclaredParameters;
    }

    /**
     *
     * @param description
     * @return
     */
    public TaskDescription setDescription(String description)
    {
        this.description = description;
        return this;
    }

    /**
     *
     * @param name
     * @param type
     * @return
     */
    public TaskDescription addArgument(String name, InputType type)
    {
        return this.addArgument(new ArgumentDescription(name, type));
    }

    /**
     *
     * @param name
     * @param type
     * @param defaultValue
     * @return
     */
    public TaskDescription addArgument(String name, InputType type, String defaultValue)
    {
        return this.addArgument(new ArgumentDescription(name, type).setDefaultValue(defaultValue));
    }

    /**
     *
     * @param description
     * @return
     */
    public TaskDescription addArgument(ArgumentDescription description)
    {
        this.arguments.add(description);
        return this;
    }

    /**
     *
     * @param parameter
     * @param name
     * @param type
     * @return
     */
    public TaskDescription addParameter(String parameter, String name, InputType type)
    {
        return this.addParameter(parameter, new ParameterDescription(name, type));
    }

    /**
     *
     * @param parameter
     * @param name
     * @param type
     * @param defaultValue
     * @return
     */
    public TaskDescription addParameter(String parameter, String name, InputType type, String defaultValue)
    {
        return this.addParameter(parameter, new ParameterDescription(name, type).setDefaultValue(defaultValue));
    }

    /**
     *
     * @param parameter
     * @param description
     * @return
     */
    public TaskDescription addParameter(String parameter, ParameterDescription description)
    {
        this.parameters.put(parameter, description);
        return this;
    }

    /**
     *
     * @return
     */
    public TaskDescription enableUndeclaredParameters()
    {
        this.allowUndeclaredParameters = true;
        return this;
    }

    /**
     *
     * @return
     */
    public TaskDescription disableUndeclaredParameters()
    {
        this.allowUndeclaredParameters = false;
        return this;
    }
}
