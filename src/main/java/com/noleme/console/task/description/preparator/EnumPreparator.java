package com.noleme.console.task.description.preparator;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class EnumPreparator <E extends Enum> implements InputPreparator
{
    private Class<E> type;

    /**
     *
     * @param type
     */
    public EnumPreparator(Class<E> type)
    {
        this.type = type;
    }

    /**
     * @param input
     * @return
     * @throws InputPreparationException
     */
    @Override
    public Object prepare(String input) throws InputPreparationException
    {
        for (Object entry : this.type.getEnumConstants())
        {
            E enumEntry = (E)entry;
            if (enumEntry.name().equals(input))
                return enumEntry;
            if (enumEntry.name().toLowerCase().equals(input))
                return enumEntry;
        }
        throw new InputPreparationException("No "+input+" entry could be found in "+this.type.getName()+".");
    }
}
