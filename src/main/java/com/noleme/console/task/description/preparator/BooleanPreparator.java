package com.noleme.console.task.description.preparator;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class BooleanPreparator implements InputPreparator
{
    /**
     * @param input
     * @return
     * @throws InputPreparationException
     */
    @Override
    public Object prepare(String input) throws InputPreparationException
    {
        if (input.equals("true"))
            return true;
        else if (input.equals("false"))
            return false;
        throw new InputPreparationException("The provided parameter "+input+" is expected to be either \"true\" or \"false\".");
    }
}
