package com.noleme.console.task.description.preparator;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class IntegerPreparator implements InputPreparator
{
    /**
     * @param input
     * @return
     * @throws InputPreparationException
     */
    @Override
    public Object prepare(String input) throws InputPreparationException
    {
        return Integer.parseInt(input);
    }
}
