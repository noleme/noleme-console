package com.noleme.console.task.description;

import com.noleme.console.input.InputType;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/12/2017
 */
public class ArgumentDescription extends InputDescription<ArgumentDescription>
{
    /**
     *
     * @param name
     * @param type
     */
    public ArgumentDescription(String name, InputType type)
    {
        super(name, type);
    }
}
