package com.noleme.console.task;

import com.noleme.commons.stream.Streams;
import com.noleme.console.Console;
import com.noleme.console.input.Input;
import com.noleme.console.input.InputType;
import com.noleme.console.output.Output;
import com.noleme.console.task.description.ArgumentDescription;
import com.noleme.console.task.description.ParameterDescription;
import com.noleme.console.task.description.TaskDescription;
import com.noleme.console.task.description.preparator.BooleanPreparator;
import com.noleme.console.task.exception.TaskException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 17/02/2018
 */
public class Shell implements Task
{
    @Override
    public TaskDescription setup()
    {
        return new TaskDescription()
            .setDescription("Runs a shell command from the provided arguments.")
            .addArgument(new ArgumentDescription("args", InputType.REQUIRED).setVariable(true))
            .addParameter("inherit-io", new ParameterDescription("inherit_io", InputType.OPTIONAL)
                .setDefaultValue("false")
                .setPreparator(new BooleanPreparator())
            )
        ;
    }

    @Override
    public void run(Console console, Input input, Output output) throws TaskException
    {
        String[] args = this.prepareArgs(input);

        try {
            output.line.info(this.collapse(args));

            ProcessBuilder cmd = new ProcessBuilder().command(args);

            if (input.parameters.getBoolean("inherit_io"))
                cmd.inheritIO();

            Process process = cmd.start();
            process.waitFor();

            ByteArrayOutputStream os = new ByteArrayOutputStream();

            Streams.flow(process.getInputStream(), os);
            Streams.flow(process.getErrorStream(), os);

            output.print(new String(os.toByteArray(), Charset.forName("UTF-8")));
        }
        catch (InterruptedException | IOException e) {
            throw new TaskException("An unexpected error occurred while running a shell command ("+this.collapse(args)+")", e);
        }
    }

    /**
     *
     * @param input
     * @return
     */
    private String[] prepareArgs(Input input)
    {
        List<String> finalArgs = new ArrayList<>();

        for (String arg : (Collection<String>)input.arguments.getCollection("args"))
            Collections.addAll(finalArgs, arg.trim().split(" "));

        return finalArgs.toArray(new String[finalArgs.size()]);
    }

    /**
     *
     * @param args
     * @return
     */
    private String collapse(String[] args)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0 ; i < args.length ; ++i)
        {
            sb.append(args[i]);
            if (i < (args.length - 1))
                sb.append(" ");
        }
        return sb.toString();
    }
}
