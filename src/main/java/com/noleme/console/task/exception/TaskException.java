package com.noleme.console.task.exception;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 29/04/2017
 */
public class TaskException extends Exception
{
    /**
     *
     * @param message
     */
    public TaskException(String message)
    {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public TaskException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
