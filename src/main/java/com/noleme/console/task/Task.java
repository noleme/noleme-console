package com.noleme.console.task;

import com.noleme.console.Console;
import com.noleme.console.input.Input;
import com.noleme.console.output.Output;
import com.noleme.console.task.description.TaskDescription;
import com.noleme.console.task.exception.TaskException;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 29/04/2017
 */
public interface Task
{
    /**
     *
     * @return
     */
    TaskDescription setup();

    /**
     *
     * @param console
     * @param input
     * @param output
     * @throws TaskException
     */
    void run(Console console, Input input, Output output) throws TaskException;
}
