package task;

import com.noleme.console.Console;
import com.noleme.console.input.Input;
import com.noleme.console.input.InputType;
import com.noleme.console.output.Output;
import com.noleme.console.task.Task;
import com.noleme.console.task.description.ArgumentDescription;
import com.noleme.console.task.description.ParameterDescription;
import com.noleme.console.task.description.TaskDescription;
import com.noleme.console.task.description.preparator.BooleanPreparator;
import com.noleme.console.task.description.preparator.EnumPreparator;
import com.noleme.console.task.description.preparator.IntegerPreparator;
import com.noleme.console.task.description.validator.EnumValidator;
import com.noleme.console.task.description.validator.IntegerValidator;
import task.model.TestEnum;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 03/12/2017
 */
public class ParameterTask implements Task
{
    @Override
    public TaskDescription setup()
    {
        return new TaskDescription()
            .setDescription("Some description")
            //.addArgument("a1", InputType.REQUIRED)
            //.addArgument(new ArgumentDescription("a2", InputType.OPTIONAL))
            .addArgument(new ArgumentDescription("a3", InputType.OPTIONAL).setVariable(true))
            .addParameter("param1", "p1", InputType.REQUIRED)
            .addParameter("param2", "p2", InputType.OPTIONAL, "\t")
            .addParameter("param3", new ParameterDescription("p3", InputType.OPTIONAL)
                .setDefaultValue("three")
                .setPreparator(new EnumPreparator<>(TestEnum.class))
                .setValidator(new EnumValidator<>(TestEnum.TWO, TestEnum.THREE))
            )
            .addParameter("param4", new ParameterDescription("p4", InputType.OPTIONAL)
                .setDefaultValue("false")
                .setPreparator(new BooleanPreparator()))
            .addParameter("param5", new ParameterDescription("p5", InputType.OPTIONAL)
                .setPreparator(new IntegerPreparator())
                .setValidator(new IntegerValidator.Higher(3))
                .setVariable(true)
            )
            .addParameter("param6", new ParameterDescription("p6", InputType.OPTIONAL)
                .setPreparator(new IntegerPreparator())
                .setValidator(new IntegerValidator.HigherEqual(2), new IntegerValidator.LowerEqual(5))
            )
        ;
    }

    @Override
    public void run(Console console, Input input, Output output)
    {
        output.println(input.arguments.getString("a1"));
        output.println(input.arguments.getString("a2"));
        output.println(input.arguments.get("a3"));
        output.println(input.parameters.getString("p1"));
        output.println(input.parameters.getString("p2"));
        output.println(input.parameters.getEnum("p3", TestEnum.class));
        output.println(input.parameters.getBoolean("p4"));
        output.println(input.parameters.getCollection("p5"));
        output.println(input.parameters.getInteger("p6"));
    }
}
