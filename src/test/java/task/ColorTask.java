package task;

import com.noleme.console.Console;
import com.noleme.console.input.Input;
import com.noleme.console.output.Output;
import com.noleme.console.output.style.ConsoleColor;
import com.noleme.console.task.Task;
import com.noleme.console.task.description.TaskDescription;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 03/12/2017
 */
public class ColorTask implements Task
{
    @Override
    public TaskDescription setup()
    {
        return null;
    }

    @Override
    public void run(Console console, Input input, Output output)
    {
        output.color.println("Red test.", ConsoleColor.RED);
        output.color.println("Blue test.", ConsoleColor.BLUE);
        output.color.println("", ConsoleColor.BLACK, ConsoleColor.Background.WHITE);

        output.color.println("Bold yellow", ConsoleColor.YELLOW_BOLD);
        output.color.println("Underlined cyab", ConsoleColor.CYAN_UNDERLINED);
        output.color.println("Bright red", ConsoleColor.RED_BRIGHT);

        output.block.info("info message");
        output.block.success("success message");
        output.block.warning("warning message");
        output.block.error("error message");
        output.block.block("custom message", ConsoleColor.PURPLE);

        output.line.info("info message");
        output.line.success("success message");
        output.line.warning("warning message");
        output.line.error("error message");
        output.line.line("custom message", ConsoleColor.PURPLE);
    }
}
