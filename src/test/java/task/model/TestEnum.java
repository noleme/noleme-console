package task.model;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 03/12/2017
 */
public enum TestEnum
{
    ONE,
    TWO,
    THREE;
}
