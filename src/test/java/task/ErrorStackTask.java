package task;

import com.noleme.console.Console;
import com.noleme.console.input.Input;
import com.noleme.console.output.Output;
import com.noleme.console.task.Task;
import com.noleme.console.task.description.TaskDescription;
import com.noleme.console.task.exception.TaskException;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 27/10/2017
 */
public class ErrorStackTask implements Task
{
    @Override
    public TaskDescription setup()
    {
        return null;
    }

    @Override
    public void run(Console console, Input input, Output output) throws TaskException
    {
        try {
            Integer.parseInt("abcde");
        }
        catch (NumberFormatException e) {
            throw new TaskException("An error occurred, and for a good reason.", e);
        }
    }
}
