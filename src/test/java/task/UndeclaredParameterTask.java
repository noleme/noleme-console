package task;

import com.noleme.console.Console;
import com.noleme.console.input.Input;
import com.noleme.console.input.InputType;
import com.noleme.console.output.Output;
import com.noleme.console.task.Task;
import com.noleme.console.task.description.TaskDescription;

/**
 * @author Pierre Lecerf (plecerf@lumiomedical.com)
 * Created on 2020/07/03
 */
public class UndeclaredParameterTask implements Task
{
    @Override
    public TaskDescription setup()
    {
        return new TaskDescription()
            .setDescription("Some description")
            .addParameter("param1", "p1", InputType.REQUIRED)
            .enableUndeclaredParameters()
        ;
    }

    @Override
    public void run(Console console, Input input, Output output)
    {
        output.line.info(input.parameters.getString("p1"));
        input.undeclaredParameters.getProperties().forEach((key, value) -> {
            output.line.info(key+": "+value);
        });
    }
}
