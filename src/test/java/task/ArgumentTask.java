package task;

import com.noleme.console.Console;
import com.noleme.console.input.Input;
import com.noleme.console.input.InputType;
import com.noleme.console.output.Output;
import com.noleme.console.task.Task;
import com.noleme.console.task.description.ArgumentDescription;
import com.noleme.console.task.description.TaskDescription;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 27/10/2017
 */
public class ArgumentTask implements Task
{
    @Override
    public TaskDescription setup()
    {
        return new TaskDescription()
            .addArgument("arg1", InputType.REQUIRED)
            .addArgument("arg2", InputType.REQUIRED)
            .addArgument(new ArgumentDescription("arg3", InputType.OPTIONAL).setDefaultValue("123"))
        ;
    }

    @Override
    public void run(Console console, Input input, Output output)
    {
        output.line.info(input.arguments.getString("arg1"));
        output.line.info(input.arguments.getString("arg2"));
        output.line.info(input.arguments.getString("arg3"));
    }
}
