package task;

import com.noleme.console.Console;
import com.noleme.console.exception.ConsoleException;
import com.noleme.console.input.Input;
import com.noleme.console.output.Output;
import com.noleme.console.task.Task;
import com.noleme.console.task.description.TaskDescription;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 17/02/2018
 */
public class SubTask implements Task
{
    @Override
    public TaskDescription setup()
    {
        return null;
    }

    @Override
    public void run(Console console, Input input, Output output)
    {
        try {
            console.run("test:success");
        }
        catch (ConsoleException e) {
            e.printStackTrace();
        }
    }
}
