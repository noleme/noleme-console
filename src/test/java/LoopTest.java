import com.noleme.console.Console;
import com.noleme.console.context.Context;
import com.noleme.console.input.Input;
import com.noleme.console.input.InputType;
import com.noleme.console.loop.ArgumentProvider;
import com.noleme.console.loop.Loop;
import com.noleme.console.loop.LoopException;
import com.noleme.console.loop.TimeUnit;
import com.noleme.console.loop.sync.SyncLoop;
import com.noleme.console.loop.task.ConsumableTask;
import com.noleme.console.loop.task.LoopingTask;
import com.noleme.console.output.Output;
import com.noleme.console.task.Task;
import com.noleme.console.task.description.TaskDescription;

/**
 * @author eledhwen
 * Created on 24/10/2018
 */
public class LoopTest
{
    public static void main(String[] args) throws LoopException
    {
        Console console = new Console()
            .setTask("test", new Task() {
                @Override
                public TaskDescription setup()
                {
                    return new TaskDescription().addArgument("arg", InputType.OPTIONAL, "default");
                }

                @Override
                public void run(Console console, Input input, Output output)
                {
                    try {
                        output.println("Test "+input.arguments.getString("arg"));
                        Thread.sleep(5000);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            })
        ;

        Loop loop = new SyncLoop(console, new Context()) {
        //Loop loop = new AsyncLoop(console, new Context(), 3, 500, false) {
            @Override protected boolean loopStart() {
                System.out.println("---");
                return super.loopStart();
                //return false;
            }
        };

        loop
            .register(new LoopingTask("test", ArgumentProvider.provide("tick-1"), 1))
            .register(new LoopingTask("test", ArgumentProvider.provide("tick-3"), 3).setDelay(0, TimeUnit.SECONDS))
            .register(new LoopingTask("test", ArgumentProvider.provide("tick-6"), 6).setDelay(3, TimeUnit.SECONDS))
            .register(new LoopingTask("test", ArgumentProvider.provide("tick-60"), 1, TimeUnit.MINUTES))
            .register(new ConsumableTask("test", ArgumentProvider.provide("tick-consumable-3"), 3, 3))
            //.register(new ConsumableTask("launch:server", ArgumentProvider.provideEmpty(), 1))
        ;

        loop.start();
    }
}
