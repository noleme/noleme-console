import com.noleme.console.Console;
import com.noleme.console.ConsoleRunner;
import com.noleme.console.task.Shell;
import com.noleme.console.task.Stats;
import task.*;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 27/10/2017
 */
public class ConsoleTest
{
    /**
     *
     * @param args
     */
    public static void main(String[] args)
    {
        int status = ConsoleRunner.run(new Console()
            .setTask("test:success", new SuccessTask())
            .setTask("test:sub", new SubTask())
            .setTask("test:argument", new ArgumentTask())
            .setTask("test:parameter", new ParameterTask())
            .setTask("test:undeclared-parameter", new UndeclaredParameterTask())
            .setTask("test:error", new ErrorTask())
            .setTask("test:error-stack", new ErrorStackTask())
            .setTask("test:deprecated", new DeprecatedTask())
            .setTask("test:color", new ColorTask())
            .setTask("shell", new Shell())
            .setTask("stats", new Stats())
        , args);

        System.exit(status);
    }
}
